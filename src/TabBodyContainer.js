import React from "react";
import _ from 'lodash'
import SubtabHeader from "./SubtabHeader";
import SubtabBody from "./SubtabBody";

class TabBody extends React.Component {
  constructor () {
    super()
    this.state = {
      currentSubtab: 0
    }
  }

  changeSubtab = (e, i) => {
    e.preventDefault();
    this.setState({
      currentSubtab: i
    });
  }

  render () {
    return (
      <div>
        {this.props.tabs.map(
          (tab, i) => (this.props.currentTab === i) && (
            <div key={i}>
              <div>
                {tab.name}
                {tab.content.filter(el => el.type === 'sub-tab').length > 0
                ? (
                  <div style={{display: 'flex'}}>
                    <SubtabHeader tabs={tab.content} changeTab={this.changeSubtab} />
                    <SubtabBody tabs={tab.content} currentStab={this.state.currentSubtab} />
                  </div>
                ) : (
                  <div>
                    contenu sans sous tab
                  </div>
                )}
              </div>
            </div>
          )
        )}
      </div>
    );

  }
}

export default TabBody;
