import React from "react";
import TabHeader from "./TabHeader";
import TabBody from "./TabBodyContainer";

class TabContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      currentTab: 0,
      tabs: [
        {
          name: "general.informations",
          type: "tab",
          content: [
            {
              name: "name",
              type: "string",
              option: {
                required: true
              }
            },
            {
              name: "zipcode",
              type: "string",
              option: {
                required: true,
                attr: {
                  pattern: "[0-9]{5}"
                }
              }
            },
            {
              name: "city",
              type: "string",
              option: {
                required: true
              }
            },
            {
              name: "address",
              type: "string",
              option: {
                required: false
              }
            },
            {
              name: "buildingPermitDepositDate",
              type: "date",
              option: {
                required: true
              }
            },
            {
              name: "shippingDate",
              type: "date",
              option: {
                required: true
              }
            }
          ]
        },
        {
          name: "programDefinition",
          type: "tab",
          content: [
            {
              name: "usesCreation",
              type: "sub-tab",
              content: [
                {
                  name: "shab",
                  type: "shab",
                  option: {
                    required: true
                  }
                },
                {
                  name: "usesDistribution",
                  type: "usesDistribution",
                  option: {
                    required: true
                  }
                }
              ]
            },
            {
              name: "landFeatures",
              type: "sub-tab",
              content: [
                {
                  name: "area",
                  type: "number",
                  option: {
                    required: true
                  }
                },
                {
                  name: "convertedArea",
                  type: "number",
                  option: {
                    required: true
                  }
                },
                {
                  name: "greenSpace",
                  type: "number",
                  option: {
                    required: true,
                    unit: "%;"
                  }
                },
                {
                  name: "additionnalCost",
                  type: "additionnalCost",
                  options: {
                    choices: {
                      Demolition: "Demolition",
                      "Sol dur": "hard",
                      "Construction contre mitoyens": "adjoiningConstruction",
                      "Parois spéciales": "specialWalls",
                      "Fondations spéciales": "specialFoundations",
                      "Présence d'eau": "water",
                      "Polution des sols": "polution"
                    },
                    unit: "€",
                    multiple: true,
                    trans_key: "form.surcout",
                    trans_var: "name"
                  }
                }
              ]
            }
          ]
        },
        {
          name: "first.accessor",
          type: "tab",
          content: [
            {
              name: "shabReminder",
              type: "shabReminder",
              option: {
                required: true
              }
            },
            {
              name: "housingTypology",
              type: "housingTypology",
              option: {
                required: true
              }
            },
            {
              name: "floorNumbers",
              type: "number",
              option: {
                required: true
              }
            },
            {
              name: "stairNumber",
              type: "number",
              option: {
                required: true
              }
            },
            {
              name: "undergroundLevelNumber",
              type: "number",
              option: {
                required: true
              }
            },
            {
              name: "technicalData",
              type: "technicalData",
              option: {
                required: true
              }
            }
          ]
        },
        {
          name: "investor",
          type: "tab",
          content: [
            {
              name: "shabReminder",
              type: "shabReminder",
              option: {
                required: true
              }
            },
            {
              name: "housingTypology",
              type: "housingTypology",
              option: {
                required: true
              }
            },
            {
              name: "floorNumbers",
              type: "number",
              option: {
                required: true
              }
            },
            {
              name: "stairNumber",
              type: "number",
              option: {
                required: true
              }
            },
            {
              name: "undergroundLevelNumber",
              type: "number",
              option: {
                required: true
              }
            },
            {
              name: "technicalData",
              type: "technicalData",
              option: {
                required: true
              }
            }
          ]
        },
        {
          name: "social.housing",
          type: "tab",
          content: [
            {
              name: "shabReminder",
              type: "shabReminder",
              option: {
                required: true
              }
            },
            {
              name: "housingTypology",
              type: "housingTypology",
              option: {
                required: true
              }
            },
            {
              name: "floorNumbers",
              type: "number",
              option: {
                required: true
              }
            },
            {
              name: "stairNumber",
              type: "number",
              option: {
                required: true
              }
            },
            {
              name: "undergroundLevelNumber",
              type: "number",
              option: {
                required: true
              }
            },
            {
              name: "technicalData",
              type: "technicalData",
              option: {
                required: true
              }
            }
          ]
        }
      ]
    };
  }

  changeTab = (e, i) => {
    e.preventDefault();
    this.setState({
      currentTab: i
    });
  }

  shoouldComponentUpdate(nextProps, nextState) {
    return nextState.currentTab !== this.state.currentTab;
  }

  render() {
    return (
      <div style={tabs}>
        <TabHeader {...this.state} changeTab={this.changeTab}/>
        <TabBody {...this.state} />
      </div>
    );
  }
}

const tabs = {
  display: "flex",
  flexFlow: "column nowrap"
};

export default TabContainer;
